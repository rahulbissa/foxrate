angular.module('myApp', ['ionic'])

.controller('MyCtrl', function($scope,$http) {
  
	
  $scope.submit=function(isValid){
    if(isValid){
      $scope.submitted=false;
	  var data=$scope.user;
	  console.log(data);
	  $http.post('/angular/submit.php',data).
	  success(function(data, status, headers, config) {
		 console.log(data);
  		 $scope.message = data.message;
		 if(data.status){
			 $scope.user={};
		 }
	  }).
	  error(function(data, status, headers, config) {
		  console.log(data);
		  $scope.error = data.message;
	  });
    }
  };

})
.directive('jbRating', function () {
    return {
      restrict: 'A',
      require: 'ngModel',
      template: '<ul class="rating">' +
                  '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
                    '\u2605' +
                  '</li>' +
                '</ul>',
      scope: {
        max: '=',
        readonly: '@',
        onRatingSelected: '&'
      },
      link: function (scope, elem, attrs, ngModel) {
        
        var updateStars = function() {
          scope.stars = [];
          for (var  i = 0; i < scope.max; i++) {
            scope.stars.push({filled: i < ngModel.$modelValue});
          }
        };
        
        var originalIsEmpty = ngModel.$isEmpty;
        
        ngModel.$isEmpty = function(value) {
          return value === 0 || originalIsEmpty.call(ngModel,value);
        };

        ngModel.$render = function() {
          updateStars();
        };

        scope.toggle = function(index) {
          if (scope.readonly && scope.readonly === 'true') {
            return;
          }
          ngModel.$setViewValue(index + 1);
          updateStars();
          scope.onRatingSelected({rating: index + 1});
        };
      }
    };
});
