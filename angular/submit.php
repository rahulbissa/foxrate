<?php
header("content-type:application/json");
$language = file_get_contents("language.json");
$language = json_decode($language);
$response = array();
$received =  file_get_contents('php://input');
$_POST = json_decode($received,true);
if(isset($_POST)){
	
	if(isset($_POST["rating"]) && $_POST["rating"]>3){
		$response["status"] = true;
		$response["message"] = $language->en->success;
	}else{
		$response["status"] = false;
		$response["message"] = $language->en->error;
	}
}else{
	$response["status"] = false;
	$response["message"] = $language->en->error;
}
echo json_encode($response);